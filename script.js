let dataList = [];

// Show form daftar
function showFormDaftar() {
  document.getElementById("form-daftar").style.display = "block";
}

// Close form daftar
function closeFormDaftar() {
  document.getElementById("form-daftar").style.display = "none";
}

// Menambahkan data
function addData() {
  let nama = document.getElementById("input-nama");
  let hasilNama = nama.value.trim().toUpperCase();

  let noKtp = document.getElementById("input-noKtp");
  let hasilNoKtp = noKtp.value.trim().toUpperCase();

  if (hasilNama !== "" && hasilNoKtp !== "") {
    if (hasilNoKtp.length <= 16) {
      if (dataList.find((data) => data.NoKtp === hasilNoKtp)) {
        alert("Nomor KTP sudah terdaftar sebelumnya!");
      } else {
        dataList.push({ Nama: hasilNama, NoKtp: hasilNoKtp });
        closeFormDaftar();
        nama.value = "";
        noKtp.value = "";
        showData();
        document.getElementById("notification").style.display = "none";
        console.log(dataList);
      }
    } else {
      document.getElementById("notification").style.display = "block";
    }
  }
}

// Tampilkan data
function showData() {
  let tableBody = document.getElementById("table-body");
  tableBody.innerHTML = "";

  for (let i = 0; i < dataList.length; i++) {
    let data = `
        <tr>
          <td>${dataList[i].Nama}</td>
          <td>${dataList[i].NoKtp}</td>
          <td class="text-center d-flex gap-3 justify-content-center">
            <button id="btn-show-data" class="btn btn-light" onclick="showFormEdit(${i})"><i class="fa-solid fa-pen"></i></button>
            <button id="btn-delete-data" class="btn btn-light" onclick="deleteData(${i})"><i class="fa-regular fa-trash-can"></i></button>
          </td>
        </tr>
          `;
    tableBody.insertAdjacentHTML("beforeend", data);
  }
}

// Show form edit
function showFormEdit(index) {
  document.getElementById("form-edit").style.display = "block";

  let nama = document.getElementById("input-edit-nama");
  let noKtp = document.getElementById("input-edit-noKtp");

  nama.value = dataList[index].Nama;
  noKtp.value = dataList[index].NoKtp;

  nama.dataset.index = index;
}

// Save Edit
async function saveEdit() {
  let nama = document.getElementById("input-edit-nama");
  let hasilEditNama = nama.value.trim().toUpperCase();

  let noKtp = document.getElementById("input-edit-noKtp");
  let hasilEditNoKtp = noKtp.value.trim();

  let index = parseInt(nama.dataset.index);

  if (hasilEditNama !== "" && hasilEditNoKtp !== "") {
    if (hasilEditNoKtp.length <= 16) {
      if (dataList.find((data) => data.NoKtp === hasilEditNoKtp)) {
        alert("Nomor KTP sudah terdaftar sebelumnya!");
      } else {
        dataList[index].Nama = hasilEditNama;
        dataList[index].NoKtp = hasilEditNoKtp;
        showData();
        closeFormEdit();
        document.getElementById("notification-edit").style.display = "none";
      }
    } else {
      document.getElementById("notification-edit").style.display = "block";
    }
  } else {
    alert("Nama dan Nomor KTP harus diisi!");
  }
}

// Close Form Edit
function closeFormEdit() {
  document.getElementById("form-edit").style.display = "none";
}



// Hapus Data
function deleteData(index) {
  document.getElementById("confirmation").style.display = "block";
  document.getElementById("confirmation").setAttribute("data-index", index);
}

// Popup Confirmation Hapus
function confirmDelete() {
  let index = parseInt(
    document.getElementById("confirmation").getAttribute("data-index")
  );

  dataList.splice(index, 1);

  document.getElementById("confirmation").style.display = "none";

  showData();
  console.log(dataList);
}

// Popup Confirmation Batal
function cancelDelete() {
  document.getElementById("confirmation").style.display = "none";
}

// Search Data
function filterData() {
  return new Promise((resolve, reject) => {
    let keyword = document.getElementById("search").value.trim().toLowerCase();

    if (keyword === "") {
      showData();
    } else {
      let filteredData = dataList.filter(
        (item) =>
          item.Nama.toLowerCase().includes(keyword) ||
          item.NoKtp.toString().includes(keyword)
      );

      let tableBody = document.getElementById("table-body");
      tableBody.innerHTML = "";

      for (let i = 0; i < filteredData.length; i++) {
        let data = `
          <tr>
            <td>${filteredData[i].Nama}</td>
            <td>${filteredData[i].NoKtp}</td>
            <td class="text-center d-flex gap-3 justify-content-center">
              <button id="btn-show-data" class="btn btn-light" onclick="showFormEdit(${i})"><i class="fa-solid fa-pen"></i></button>
              <button id="btn-delete-data" class="btn btn-light" onclick="deleteData(${i})"><i class="fa-regular fa-trash-can"></i></button>
            </td>
          </tr>
        `;
        tableBody.insertAdjacentHTML("beforeend", data);
      }
    }

    resolve();
  });
}

// Fungsi Enter Filter data
function enterFilterData(event) {
  if (event.keyCode === 13) {
    filterData();
  }
}

// Fungsi Enter Add Data
function enterAddData(event) {
  if (event.keyCode === 13) {
    addData();
  }
}

// Fungsi Save Edit Data
function enterEditData(event) {
  if (event.keyCode === 13) {
    saveEdit();
  }
}
